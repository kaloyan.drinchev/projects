import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";

document.querySelector("body").style.backgroundImage = 
  "linear-gradient(green, white)";
document.querySelector("body").style.backgroundSize = "cover";
document.querySelector("body").style.height = "100vh";
document.querySelector("body").style.backgroundRepeat = "no-repeat";
document.querySelector("body").style.backgroundPosition = "center";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);
