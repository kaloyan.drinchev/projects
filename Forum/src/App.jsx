/* eslint-disable no-unused-vars */
import { Routes, Route } from "react-router-dom";
import Home from "./views/Home/Home";
import Login from "./views/Login/Login";
import SignUp from "./views/SignUp/SignUp";
import CreatePost from "./views/CreatePost/CreatePost";
import AdminPanel from "./views/AdminPanel/AdminPanel";
import Discussions from "./views/Discussions/Discussions";
import YourPosts from "./views/YourPosts/YourPosts";
import SingleArticleView from "./components/LoadAdminPosts/SingleArticleView";
import LoadSearchResults from "./components/UserSearchResults/LoadSearchResults/LoadSearchResults";
import CreateAdminPost from "./views/AdminPanel/CreateAdminPost/CreateAdminPost";
import YourPostsView from "./views/YourPosts/YourPostsView";
import UserProfile from "./views/UserProfile/UserProfile";
import SingleUserView from "./views/SingleUserView/SingleUserView";
import SingleUserViewSearchResults from "./views/SingleUserView/SingleUserViewSearchResults";
import SingleUserViewPostTitle from "./views/SingleUserView/SingleUserViewPostTitle";
import SingleUserViewComments from "./views/SingleUserView/SingleUserViewComments";
import ForgotPassword from "./views/ForgotPassword/ForgotPassword";
import "./App.css";
import NavBar from "./components/NavBar/NavBar";
import { UserProvider } from "./context";
function App() {
  return (
    <UserProvider>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signUp" element={<SignUp />} />
        <Route path="/discussions" element={<Discussions />} />
        <Route path="/createPost" element={<CreatePost />} />
        <Route path="/yourPosts" element={<YourPosts />} />
        <Route path="/adminPanel" element={<AdminPanel />} />
        <Route path="/adminPost" element={<CreateAdminPost />} />
        <Route path="/article/:articleId" element={<SingleArticleView />} />
        <Route path="/posts/:postId" element={<YourPostsView />} />
        <Route path="/searchResults" element={<LoadSearchResults />} />
        <Route path="/navBar" element={<NavBar />} />
        <Route path="/profile" element={<UserProfile />} />
        <Route path="/discussions/user/:userId" element={<SingleUserView />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route
          path="/searchResults/user/:userId"
          element={<SingleUserViewSearchResults />}
        />
        <Route
          path="posts/:postId/user/:userId"
          element={<SingleUserViewPostTitle />}
        />
        <Route
          path="posts/:postId/user/:userId"
          element={<SingleUserViewComments />}
        />
      </Routes>
    </UserProvider>
  );
}

export default App;
