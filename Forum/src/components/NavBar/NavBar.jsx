/* eslint-disable no-unused-vars */

import { db } from "../../database";
import { get, ref } from "firebase/database";
import editIcon from "../../assets/edit-profile-icon.png";
import logoutIcon from "../../assets/logout-icon.png";
import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { handleSignOut } from "../../views/SignOut/SignOut";
import { useUserContext } from "../../context.jsx";
import "./NavBar.css";
const NavBar = () => {
  const { userData, setUserData } = useUserContext();
  const [loginStatus, setLoginStatus] = useState(
    localStorage.getItem("loginStatus")
  ); //zapazva user-a lognat pri refresh
  const [isAdminLogged, setAdminStatus] = useState(
    localStorage.getItem("isAdminLogged")
  );

  const [getUser, setUser] = useState({});
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const getUser = async () => {
      if (loginStatus && userData) {
        const userRef = ref(
          db,
          `users/${localStorage.getItem("currentUserUid")}`
        );
        const snapshot = await get(userRef);
        const fetchUserData = snapshot.val();
        setUserData(fetchUserData);
      }
    };
    getUser();
  }, [loginStatus]);

  return (
    <>
      <div className="nav-bar">
        <nav className="nav-menu">
          <NavLink to="/">
            <img src={"/src/assets/image.png"} className="logo" />
          </NavLink>
          <NavLink
            to="/"
            className="nav-link"
            // style={{ borderStyle: "unset", backgroundColor: "white" }}
            // id="home-btn"
          >
            Начална страница
          </NavLink>
          <NavLink
            to="/discussions"
            className="nav-link"
            // style={{ borderStyle: "unset", backgroundColor: "white" }}
          >
            Дискусии
          </NavLink>
          {isAdminLogged && (
            <NavLink
              to="/adminPanel"
              className="nav-link"
              // style={{ borderStyle: "unset", backgroundColor: "white" }}
            >
              Админ
            </NavLink>
          )}
          {!loginStatus ? (
            <NavLink
              to="/login"
              className="nav-link"
              style={{ borderStyle: "unset", backgroundColor: "white" }}
            >
              Вход
            </NavLink>
          ) : (
            <>
              <div className="menu-container">
                <div
                  className="menu-trigger"
                  onClick={() => {
                    setOpen(!open);
                  }}
                >
                  <label htmlFor="nav-link">
                    <img
                      src={userData.avatar}
                      className="nav-link"
                      id="userImg"
                      style={{ backgroundColor: "initial" }}
                    />
                  </label>
                  <div
                    className={`dropdown-menu ${open ? "active" : "inactive"}`}
                  >
                    <div className="user-additional-info">
                      <p id="names">
                        {userData.firstName} {userData.lastName}
                      </p>
                      {localStorage.getItem("isAdminLogged") && (
                        <p id="add-info">Web developer</p>
                      )}
                    </div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <img
                        src={editIcon}
                        alt="edit-profile-icon"
                        id="dropdown-icons"
                      />

                      <NavLink
                        to="/profile"
                        className="link"
                        style={{ padding: "15px 15px", marginLeft: "5px" }}
                        id="edit-pr-btn"
                      >
                        Промени
                      </NavLink>
                    </div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <img
                        src={logoutIcon}
                        alt="logout-icon"
                        id="dropdown-icons"
                      />
                      <NavLink
                        onClick={handleSignOut}
                        className="link"
                        id="exit-button"
                        style={{ padding: "15px 15px", marginLeft: "5px" }}
                      >
                        Изход
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
          {!loginStatus && (
            <NavLink
              to="/signUp"
              className="nav-link"
              style={{ borderStyle: "unset", backgroundColor: "white" }}
            >
              Регистрация
            </NavLink>
          )}
        </nav>
      </div>
    </>
  );
};
export default NavBar;
