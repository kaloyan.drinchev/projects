import './SortingModal.css'

const SortingModal = ({ isOpen, onClose, onSortOptionSelect }) => {
    return (
      <div className={`sorting-modal ${isOpen ? 'open' : ''}`}>
        <div className="modal-content">
          <h2>Сортиране на резултатите по:</h2>
          <button onClick={() => onSortOptionSelect('newestToOldest')}>Най-нови към най-стари</button>
                <button onClick={() => onSortOptionSelect('oldestToNewest')}>Най-стари към най-нови</button>
                <button onClick={() => onSortOptionSelect('likesMostToLeast')}>Най-харесвани</button>
          <button onClick={() => onSortOptionSelect('MostCommented')}>Най-коментирани</button>
          <button onClick={onClose}>Затвори</button>
        </div>
      </div>
    );
  };
  
  export default SortingModal;