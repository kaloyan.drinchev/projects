/* eslint-disable no-case-declarations */
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { get, ref } from "firebase/database";
import { db } from "../../../database";
import SearchResultsAdminFound from "./SearchResultInstances/SearchResultsAdminFound";
import SearchResultsNotFound from "./SearchResultInstances/SearchResultsNotFound";
import SearchResultsFound from "./SearchResultInstances/SearchResultsFound";
import SortingModal from "./SortingModal/SortingModal";
import "./LoadSearchResults.css";
const LoadSearchResults = () => {
  const [postsData, setPostsData] = useState([]);
  const [adminPostsData, setAdminPostsData] = useState([]);
  const [isPostFound, setPostFoundStatus] = useState(false);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const searchInput = searchParams.get("searchInput").toLowerCase().split(" ");
  const selectedMethod = searchParams.get("selectedMethod");
  const [sortingModalOpen, setSortingModalOpen] = useState(false);

  useEffect(() => {
    const findResults = async () => {
      const postsRef = ref(db, "posts");
      const snapshot = await get(postsRef);
      const adminPostsRef = ref(db, "adminPosts");
      const adminSnapshot = await get(adminPostsRef);
      const gatherPostResultsUSERS = [];
      const gatherPostResultsADMINS = [];
      const addedKeys = new Set()
      if (selectedMethod === "tags") {
        const tagsInput = searchInput;
        snapshot.forEach((post) => {
          const result = post.val().tags;
          result.forEach((element) => {
            if (tagsInput.includes(element.toLowerCase())) {
              const adjustPost = post.val();
              adjustPost.key = post.key;
              if (!addedKeys.has(adjustPost.key)) {
                addedKeys.add(adjustPost.key)
                gatherPostResultsUSERS.push(adjustPost);
              }
            }
          });
        });
        if (gatherPostResultsUSERS.length > 0) {
          setPostFoundStatus(true);
          setPostsData(gatherPostResultsUSERS);
        }
        adminSnapshot.forEach((post) => {
          const result = post.val().tags;
          result.forEach((element) => {
            if (tagsInput.includes(element.toLowerCase())) {
              const adjustPost = post.val();
              adjustPost.key = post.key;
              if (!addedKeys.has(adjustPost.key)) {
                addedKeys.add(adjustPost.key)
                gatherPostResultsADMINS.push(adjustPost);
              }
            }
          });
        });
        if (gatherPostResultsADMINS.length > 0) {
          setPostFoundStatus(true);
          setAdminPostsData(gatherPostResultsADMINS);
        }
      } else if (selectedMethod === "title") {
        const getInput = searchInput;
        snapshot.forEach((post) => {
          const result = post.val().title.toLowerCase().split(" ");
          result.forEach((element) => {
            if (getInput.includes(element)) {
              const adjustPost = post.val();
              adjustPost.key = post.key;
              if (!addedKeys.has(adjustPost.key)) {
                addedKeys.add(adjustPost.key)
                gatherPostResultsUSERS.push(adjustPost);
              }
              }
          });
        });
        if (gatherPostResultsUSERS.length > 0) {
          setPostFoundStatus(true);
          setPostsData(gatherPostResultsUSERS);
        }
        adminSnapshot.forEach((post) => {
          const result = post.val().title.toLowerCase().split(" ");
          result.forEach((element) => {
            if (getInput.includes(element)) {
              const adjustPost = post.val();
              adjustPost.key = post.key;
              if (!addedKeys.has(adjustPost.key)) {
                addedKeys.add(adjustPost.key)
                gatherPostResultsADMINS.push(adjustPost);
              }
            }
          });
        });
        if (gatherPostResultsADMINS.length > 0) {
          setPostFoundStatus(true);
          setAdminPostsData(gatherPostResultsADMINS);
        }
      }
    };
    findResults();
  }, []);

  const handleFilterOptions = () => {
    setSortingModalOpen(!sortingModalOpen);
  };

  const handleSorting = (sortOption) => {
    switch (sortOption) {
      case "newestToOldest":
        const newToOldADMIN = [...adminPostsData].sort(
          (a, b) => b.postedOn.fullDate - a.postedOn.fullDate
        );
        setAdminPostsData(newToOldADMIN);
        const newToOldUSER = [...postsData].sort(
          (a, b) => b.postedOn.fullDate - a.postedOn.fullDate
        );
        setPostsData(newToOldUSER);
        break;
      case "oldestToNewest":
        const oldToNewADMIN = [...adminPostsData].sort(
          (a, b) => a.postedOn.fullDate - b.postedOn.fullDate
        );
        setAdminPostsData(oldToNewADMIN);
        const oldToNewUSER = [...postsData].sort(
          (a, b) => a.postedOn.fullDate - b.postedOn.fullDate
        );
        setPostsData(oldToNewUSER);
        break;
      case "MostCommented":
        const MostCommented = [...postsData].sort(
          (a, b) => b.comments.length - a.comments.length
        );
        setPostsData(MostCommented);
        break;
      case "likesMostToLeast":
        const likesMostToLeastADMIN = [...adminPostsData].sort(
          (a, b) => b.likes.length - a.likes.length
        );
        setAdminPostsData(likesMostToLeastADMIN);
        const likesMostToLeastUSER = [...postsData].sort(
          (a, b) => b.likes.length - a.likes.length
        );
        setPostsData(likesMostToLeastUSER);
        break;
      default:
        break;
    }
  };

  return (
    <div className="search-results-container">
      <h1>Резултати от търсенето:</h1>
      <div className="filter-element">
        <span className="filter-button-text">Сортирай:</span>
        <button
          className="filter-icon-button"
          onClick={handleFilterOptions}
        ></button>
      </div>
      {sortingModalOpen && (
        <SortingModal
          isOpen={sortingModalOpen}
          onClose={handleFilterOptions}
          onSortOptionSelect={(sortOption) => {
            handleSorting(sortOption);
            handleFilterOptions();
          }}
        />
      )}
      <div className="articles-container">
        {isPostFound && postsData.length > 0 && adminPostsData.length > 0 && (
          <>
              <h1 >Потребителски постове: </h1>
            <div className='userSearchResults'>
              <SearchResultsFound postsData={postsData} />
            </div>
              <h1>Админски постове:</h1>
            <div className="adminSearchResult">
              <SearchResultsAdminFound adminPostsData={adminPostsData} />
            </div>
          </>
        )}

        {isPostFound && postsData.length > 0 && adminPostsData.length === 0 && (
          <>
            <h1>Потребителски постове:</h1>
          <div className='userSearchResults'>
            <SearchResultsFound postsData={postsData} />
            </div>
            </>
        )}

        {isPostFound && postsData.length === 0 && adminPostsData.length > 0 && (
          <div>
            <h1>Админски постове:</h1>
            <SearchResultsAdminFound adminPostsData={adminPostsData} />
          </div>
        )}

        {!isPostFound &&
          postsData.length === 0 &&
          adminPostsData.length === 0 && <SearchResultsNotFound />}
      </div>
    </div>
  );
};

export default LoadSearchResults;
