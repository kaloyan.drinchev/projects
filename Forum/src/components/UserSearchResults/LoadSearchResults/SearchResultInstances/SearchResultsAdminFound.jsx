import { NavLink } from "react-router-dom";
const SearchResultsAdminFound = ({ adminPostsData }) => {
  return (
    <div className="adminSearchResult">
      {adminPostsData.map((element) => (
        <article
          className="articles"
          key={element.key}
          style={{ backgroundImage: `url(${element.imageURL})` }}
        >
          <div className="innerElement">
            <NavLink to={`/article/${element.key}`} state={element}>
              {element.title}
            </NavLink>
            <div>Публикувано от: Admin</div>
            <div>
              Дата:{element.postedOn.date} {element.postedOn.time}
            </div>
            <div className="tags">Тагове: {element.tags.join(" ")}</div>
            <div className="likes">
              <span className="icon">👍 {element.likes.length - 1}</span>
            </div>
          </div>
        </article>
      ))}
    </div>
  );
};

export default SearchResultsAdminFound;
