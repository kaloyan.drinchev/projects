const SearchResultsNotFound = () => {
    return <h1>Няма намерени резултати от търсенето! 😢</h1>;
  };
  
  export default SearchResultsNotFound