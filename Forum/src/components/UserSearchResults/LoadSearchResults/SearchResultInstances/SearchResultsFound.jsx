/* eslint-disable react/prop-types */
import { NavLink } from "react-router-dom";

const SearchResultsFound = ({ postsData }) => {
  return (
    <>
      {Array.isArray(postsData) ? (
        postsData.map((element) => {
          return (
            <article className="single-result-post" key={element.key}>
              <div id="title">
                <NavLink to={`/posts/${element.key}`} state={element}>
                  {element.title}
                </NavLink>
                <br />
                <NavLink to={`user/${element.userID}`} state={element.userID}>
                  <img src={element.avatar} className="post-avatar"></img>{" "}
                  {element.owner}
                </NavLink>
                <div id="search-result-content-appearance">{element.content}</div>
                <div className="tags">Тагове: {element.tags.join(", ")}</div>
                <div className="likes-comments">
                  <span className="icon">👍 {element.likes.length - 1}</span>

                  <span className="icon">💬 {element.comments.length - 1}</span>
                </div>
              </div>
            </article>
          );
        })
      ) : (
        <div>Loading...</div>
      )}
    </>
  );
};

export default SearchResultsFound;
