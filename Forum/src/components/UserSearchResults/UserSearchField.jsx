import { useState } from 'react';
import { NavLink } from 'react-router-dom';
import './UserSearchField.css';

const UserSearchField = () => {
  const [searchInput, setSearchInput] = useState('');
  const [selectedMethod, setSelectedMethod] = useState('');

  const isSearchInputEmpty = searchInput.trim() === '';
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const handleSearchClick = () => {
    if (isSearchInputEmpty) {
      setShowErrorMessage(true);
    } else {
      setShowErrorMessage(false);
    }
  };
  return (
    <div className="searchBox">
      <div className="searchContainerUser">
        <div className="searchTitleUser">Търси теми</div>
        <div className="select-field-user">
          <label className="label">Търсене по:</label>
          <select
            className="select-input-user"
            value={selectedMethod}
            onChange={(e) => setSelectedMethod(e.target.value)}
          >
            <option value="" disabled>
              Избери опция!
            </option>
            <option value="title">Име на тема</option>
            <option value="tags">Тагове</option>
          </select>
        </div>
        <div className="search-input-container-user">
          <input
            placeholder="Търси..."
            onChange={(e) => setSearchInput(e.target.value)}
            className="search-input-user"
          />
          {isSearchInputEmpty && showErrorMessage && (
            <div className="error-message">Моля, въведете данни за търсене!</div>
          )}
        </div>
        {isSearchInputEmpty ? (
          <button className="search-btn-user" onClick={handleSearchClick}>
            Търси
          </button>
        ) : (
          <NavLink
            to={{
              pathname: '/searchResults',
              search: `?searchInput=${encodeURIComponent(
                searchInput
              )}&selectedMethod=${encodeURIComponent(selectedMethod)}`,
            }}
            className="search-btn-user"
          >
            Търси
          </NavLink>
        )}
      </div>
    </div>
  );
};

export default UserSearchField;