import { useState, useEffect } from "react";
import { db } from "../../database";
import { get, ref } from "firebase/database";
import "./LoadAdminPosts.css";
import { NavLink } from "react-router-dom";
const LoadAdminPosts = () => {
  const [adminsPostsCollection, setAdminPostsCollection] = useState("");

  useEffect(() => {
    const getAdminPosts = async () => {
      try {
        const postsRef = ref(db, `adminPosts`);
        const snapshot = await get(postsRef);
        const gatheredPosts = [];
        snapshot.forEach((postsSnapshot) => {
          const posts = postsSnapshot.val();
          posts.key = postsSnapshot.key;
          gatheredPosts.push(posts);
        });
        setAdminPostsCollection(gatheredPosts);
      } catch (e) {
        alert(e);
      }
    };

    getAdminPosts();
  }, []);

  return (
    <div className="articles-container-homePage">
      {Array.isArray(adminsPostsCollection) ? (
        adminsPostsCollection.map((element) => {
          return (
            <article
              className="articles"
              key={element.key}
              style={{ backgroundImage: `url(${element.imageURL})` }}
            >
              <div className="innerElement">
                <NavLink to={`/article/${element.key}`} state={element}>
                  {element.title}
                </NavLink>
                <div>Публикувано от: Admin</div>
                <div>
                  Дата:{element.postedOn.date} {element.postedOn.time}
                </div>
                <div className="tags">Тагове: {element.tags.join(", ")}</div>
                <div className="likes">
                  <span className="icon">👍 {element.likes.length - 1}</span>
                </div>
              </div>
            </article>
          );
        })
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default LoadAdminPosts;
