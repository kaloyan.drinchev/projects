import { useLocation, useNavigate } from "react-router-dom";
import "./SingleArticleView.css";
import { get, ref, update } from "firebase/database";
import { db } from "../../database";
import { useEffect, useState } from "react";
const SingleArticleView = () => {
  const [isLiked, setLikeStatus] = useState(false);
  const location = useLocation();
  const articleID = location.state;
  const navigation = useNavigate()
  
  useEffect(() => {
    const checkIfLiked = async () => {
      const getPost = ref(db, `adminPosts/${articleID.key}`);
      const gatheredPost = await get(getPost);
      console.log(gatheredPost);
      if (
        gatheredPost
          .val()
          .likes.includes(localStorage.getItem("currentUserUid"))
      ) {
        setLikeStatus(true);
      }
    };
    console.log(articleID);
    checkIfLiked();
  }, []);

  const handlePostLike = async () => {
    if (!localStorage.getItem("loginStatus")) {
      navigation("/login")
      alert('Трябва да влезеш в профила си за да хересаш тази публикация!')
    } else {
      const getPost = ref(db, `adminPosts/${articleID.key}`);
      const gatheredPost = await get(getPost);
      if (
        !gatheredPost.val().likes.includes(localStorage.getItem("currentUserUid"))
      ) {
        const updatePost = gatheredPost.val().likes;
        const newPost = [...updatePost, localStorage.getItem("currentUserUid")];
        await update(getPost, { likes: newPost });
        setLikeStatus(!isLiked);
      }
    }
  };

  const handlePostUnlike = async () => {
    const getPost = ref(db, `adminPosts/${articleID.key}`);
    const gatheredPost = await get(getPost);
    const updatePost = gatheredPost.val().likes;
    const newPost = updatePost.filter(
      (like) => like !== localStorage.getItem("currentUserUid")
    );
    await update(getPost, { likes: newPost });
    setLikeStatus(!isLiked);
  };

  return (
    <div className="article">
      <div>
        <h1>{articleID.title}</h1>
        {!isLiked ? (
          <button className="likeButton" onClick={handlePostLike}>
            Харесай
          </button>
        ) : (
          <button className="unlikeButton" onClick={handlePostUnlike}>
            Харесано!
          </button>
        )}
      </div>
      <img src={articleID.imageURL} alt={articleID.title} id="admin-article-img"/>
      <div className="content">{articleID.content}</div>
    </div>
  );
};

export default SingleArticleView;
