import { getDoc, setDoc, doc, query, where } from "firebase/firestore";
import { db } from "../config/firebase-config";

export const getUserByHandle = (handle) => {
    return getDoc(doc(db, `users/${handle}`));
}

export const createUserHandle = (handle, uid, email) => {
    return setDoc(doc(db, `users/${handle}`), {
        uid,
        email,
        createdOn: Date.now()
    });
}

export const getUserData = async (uid) => {
    try {
      const usersCollectionRef = collection(db, 'users');
      const userQuery = query(usersCollectionRef, where('uid', '==', uid));
      const querySnapshot = await getDoc(userQuery);
  
      if (querySnapshot.exists()) {
        const userData = querySnapshot.data();
        return userData;
      } else {
        console.log('User not found.');
        return null;
      }
    } catch (error) {
      console.error('Error fetching user data:', error);
      return null;
    }
  };