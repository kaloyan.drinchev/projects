import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database"; 
import {getStorage} from 'firebase/storage'
const firebaseConfig = {
  apiKey: "AIzaSyDXHZ-K_-OID3u5L7FzFdWK52D-lOAqya8",
  authDomain: "team12-sforumproject.firebaseapp.com",
  databaseURL:
    "https://team12-sforumproject-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "team12-sforumproject",
  storageBucket: "team12-sforumproject.appspot.com",
  messagingSenderId: "662126960618",
  appId: "1:662126960618:web:4b3cdac8f3deac384adc93",
  measurementId: "G-Q5J8QMS7WS",
};

export const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
export const auth = getAuth(app);
export const storage = getStorage()
