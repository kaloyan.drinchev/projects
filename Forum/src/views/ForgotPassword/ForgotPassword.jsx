import { useState } from "react";
import { NavLink } from "react-router-dom";
import { resetPassword } from "../../services/auth.services";

export default function ForgotPassword() {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();

    try {
      setMessage("");
      setError("");
      await resetPassword(email);
      setMessage("Check your email box for further instructions");
    } catch {
      setError("Невалиден email адрес.");
    }
  }
  return (
    <div className="loginPage">
      <div className="loginContainer">
        {error && <div id="alert">{error}</div>}
        {message && <div id="success">{message}</div>}
        <p>Поднови паролата си</p>
        <div>
          <div>E-mail:</div>
          <input
            type="email"
            placeholder="E-mail..."
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </div>
        <button onClick={handleSubmit}>Поднови</button>
        <div id="enter-button">
          <NavLink to="/login"> Вход </NavLink>
        </div>
      </div>
    </div>
  );
}
