/* eslint-disable no-case-declarations */
import { NavLink } from "react-router-dom";
import UserSearchField from "../../components/UserSearchResults/UserSearchField";
import "./Discussions.css";
import LoadUserPosts from "./LoadUserPosts";
import "../../components/UserSearchResults/LoadSearchResults/SortingModal/SortingModal.css";
const Discussions = () => {
  return (
    <>
      {" "}
      <div id="discHeader">ДИСКУСИИ</div>
      <nav className="discussionNav">
        <div className="content">
          <UserSearchField></UserSearchField>
          <div className="button-container">
            {localStorage.getItem("loginStatus") ? (
              <NavLink to="/yourPosts">
                <span className="nav-buttons"> Твоите теми</span>
              </NavLink>
            ) : (
              <NavLink to="/login">
                <span className="nav-buttons"> Твоите теми</span>
              </NavLink>
            )}
            {localStorage.getItem("loginStatus") ? (
              <NavLink to="/createPost">
                <span className="nav-buttons">Създай тема</span>
              </NavLink>
            ) : (
              <NavLink to="/login">
                <span className="nav-buttons">Създай тема</span>
              </NavLink>
            )}
          </div>
        </div>
      </nav>
      <div className="load-posts">
        {" "}
        <LoadUserPosts />
      </div>
    </>
  );
};
export default Discussions;
