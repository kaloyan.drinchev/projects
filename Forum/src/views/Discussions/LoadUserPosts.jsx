/* eslint-disable no-case-declarations */
// import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { get, ref } from "firebase/database";
import { db } from "../../database";
import "./LoadUserPosts.css";
import ReactPaginate from "react-paginate";
import SortingModal from "../../components/UserSearchResults/LoadSearchResults/SortingModal/SortingModal";
// import SearchResultsFound from "../../components/UserSearchResults/LoadSearchResults/SearchResultInstances/SearchResultsFound";
// import { useUserContext } from "../../context";
const LoadUserPosts = () => {
  const [postsData, setPostsData] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const elementsPerPage = 5;
  const [sortingModalOpen, setSortingModalOpen] = useState(false);
  // const { userData } = useUserContext();

  useEffect(() => {
    const findResults = async () => {
      const userPosts = [];
      const postsRef = ref(db, "posts");
      const snapshot = await get(postsRef);

      snapshot.forEach((element) => {
        const adjustPost = element.val();
        adjustPost.key = element.key;
        userPosts.push(adjustPost);
      });
      setPostsData(userPosts);
    };
    findResults();
  }, []);

  const handleFilterOptions = () => {
    setSortingModalOpen(!sortingModalOpen);
  };

  const handlePageClick = (event) => {
    setCurrentPage(event.selected);
  };

  const handleSorting = (sortOption) => {
    switch (sortOption) {
      case "newestToOldest":
        const newToOldUSER = [...postsData].sort(
          (a, b) => b.postedOn.fullDate - a.postedOn.fullDate
        );
        setPostsData(newToOldUSER);
        break;
      case "oldestToNewest":
        const oldToNewUSER = [...postsData].sort(
          (a, b) => a.postedOn.fullDate - b.postedOn.fullDate
        );
        setPostsData(oldToNewUSER);
        break;
      case "MostCommented":
        const MostCommented = [...postsData].sort(
          (a, b) => b.comments.length - a.comments.length
        );

        setPostsData(MostCommented);
        break;
      case "likesMostToLeast":
        const likesMostToLeastUSER = [...postsData].sort(
          (a, b) => b.likes.length - a.likes.length
        );
        setPostsData(likesMostToLeastUSER);
        break;
      default:
        break;
    }
  };

  const pageCount = Math.ceil(postsData.length / elementsPerPage);

  return (
    <>
      <div className="filter-element">
        <span className="filter-button-text">Сортирай:</span>
        <button
          className="filter-icon-button"
          onClick={handleFilterOptions}
        ></button>
      </div>
      {sortingModalOpen && (
        <SortingModal
          isOpen={sortingModalOpen}
          onClose={handleFilterOptions}
          onSortOptionSelect={(sortOption) => {
            handleSorting(sortOption);
            handleFilterOptions();
          }}
        />
      )}
      <ReactPaginate
        nextLabel="Напред >"
        onPageChange={handlePageClick}
        forcePage={currentPage}
        pageRangeDisplayed={3}
        marginPagesDisplayed={2}
        pageCount={pageCount}
        previousLabel="< Назад"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        breakLabel="..."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="activatedPage"
        renderOnZeroPageCount={null}
      />
      <div className="mother-container" style={{ display: "inline-flex" }}>
        {Array.isArray(postsData) ? (
          postsData
            .slice(
              currentPage * elementsPerPage,
              (currentPage + 1) * elementsPerPage
            )
            .map((element) => {
              return (
                <article className="single-post" key={element.key}>
                  <div id="post-cont">
                    <NavLink to={`/posts/${element.key}`} state={element}>
                      <div id="title-post">{element.title}</div>
                    </NavLink>
                    <div id="text">
                      <NavLink
                        to={`user/${element.userID}`}
                        state={element.userID}
                      >
                        <img src={element.avatar} className="post-avatar"></img>{" "}
                        {element.owner}
                      </NavLink>
                    </div>
                    <span id="date-time">
                      {element.postedOn.date} {element.postedOn.time}
                    </span>
                    <div id="text">{element.content}</div>
                    <div className="tags">
                      Тагове: {element.tags.join(", ")}
                    </div>
                    <div className="likes-comments">
                      <span className="icon">
                        👍 {element.likes.length - 1}
                      </span>

                      <span className="icon">
                        💬 {element.comments.length - 1}
                      </span>
                    </div>
                  </div>
                </article>
              );
            })
        ) : (
          <div>Loading...</div>
        )}
      </div>
      <ReactPaginate
        nextLabel="Напред>"
        onPageChange={handlePageClick}
        forcePage={currentPage}
        pageRangeDisplayed={3}
        marginPagesDisplayed={2}
        pageCount={pageCount}
        previousLabel="< Назад"
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        breakLabel="..."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="activatedPage"
        renderOnZeroPageCount={null}
      />
    </>
  );
};

export default LoadUserPosts;
