import "./CreatePost.css";
import { useState } from "react";
import { auth, db } from "../../database";
import { ref, push, set, get } from "firebase/database";
import { useNavigate } from "react-router-dom";
import {
  MAX_LENGTH_CONTENT,
  MAX_LENGTH_TITLE,
  MIN_LENGTH_CONTENT,
  MIN_LENGTH_TITLE,
} from "../../common/constants";

const CreatePost = () => {
  const [tags, updateTags] = useState("");
  const [title, updateTitle] = useState("");
  const [content, updateContent] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const goBack = () => {
    navigate("/discussions");
  };

  const postCreator = async () => {
    const currUser = auth.currentUser;

    const userName = ref(db, `users/${localStorage.getItem("currentUserUid")}`);
    const myName = await get(userName);
    const myFullName = myName.val();

    if (myFullName.role.includes("blocked")) {
      alert("Блокиран сте.");
    } else {
      const newPost = {
        title: title,
        content: content,
        tags: tags.split(" "),
        owner: `${myFullName.firstName} ${myFullName.lastName}`,
        avatar: myFullName.avatar,
        userID: currUser.uid,
        likes: ["не може без това :("],
        comments: ["не може без това :("],
        postedOn: {
          time: new Date().toLocaleTimeString(),
          date: new Date().toLocaleDateString(),
          fullDate: new Date().getTime(),
        },
      };
      if (title === "" || content === "" || tags === "") {
        return setError("Трябва да попълните всички полета!");
      }
      if (title.length < MIN_LENGTH_TITLE || title.length > MAX_LENGTH_TITLE) {
        console.log(title.length);
        return setError(
          "Заглавието на темата трябва да е между 16 и 64 символа. "
        );
      }
      if (
        content.length < MIN_LENGTH_CONTENT ||
        content.length > MAX_LENGTH_CONTENT
      ) {
        return setError(
          "Съдържанието на темата трябва да е между 32 и 8192 символа."
        );
      }
      try {
        setError("");
        const postsRef = ref(db, "posts");
        const newPostRef = push(postsRef);
        await set(newPostRef, newPost);
        navigate("/yourPosts");
      } catch {
        setError("Неуспешно създаване на темата");
      }
    }
  };

  return (
    <>
      <div className="post-container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            {error && <div id="alert">{error}</div>}
            <h1>Създай тема</h1>
            <form action="" method="POST">
              <label htmlFor="title">
                Заглавие <span className="require">*</span>
              </label>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="title"
                  onChange={(e) => updateTitle(e.target.value)}
                />
              </div>
              <label htmlFor="description">
                Съдържание <span className="require">*</span>
              </label>
              <div className="form-group">
                <textarea
                  rows="5"
                  data-expandable
                  onChange={(e) => updateContent(e.target.value)}
                ></textarea>
              </div>
              <label htmlFor="slug">
                Тагове <span className="require">*</span>{" "}
              </label>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="slug"
                  onChange={(e) => updateTags(e.target.value)}
                />
              </div>
              <div className="form-group">
                <p>
                  <span className="require">*</span> - Задължителни полета
                </p>
              </div>
              <div className="form-group">
                <button
                  type="button"
                  className="submit-button"
                  onClick={postCreator}
                >
                  Създай
                </button>
                <button
                  className="cancel-button"
                  type="button"
                  onClick={goBack}
                >
                  Откажи
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreatePost;
