/* eslint-disable no-unused-vars */
import { useState } from "react";
import { ref, update, get } from "firebase/database";
import { db, storage } from "../../database";
import { getDownloadURL, ref as sRef, uploadBytes } from "firebase/storage";
import "./UserProfile.css";
import { useUserContext } from "../../context.jsx";
const UserProfile = () => {
  const { userData, setUserData } = useUserContext();
  const [selectedAvatar, setSelectedAvatar] = useState(null);
  const loader = document.querySelector("#loading");
  function hideLoading() {
    loader.classList.remove("display");
  }
  function displayLoading() {
    loader.classList.add("display");
  }
  const handleNewAvatar = async () => {
    if (selectedAvatar) {
      displayLoading();
      const userAvatarRef = sRef(storage, `${selectedAvatar.name}`);
      const avatarRef = ref(
        db,
        `users/${localStorage.getItem("currentUserUid")}`
      );
      const snapshot = await uploadBytes(userAvatarRef, selectedAvatar);
      const photoURL = await getDownloadURL(userAvatarRef);

      const updateData = {
        avatar: photoURL,
      };

      await update(avatarRef, updateData);

      setUserData({
        ...userData,
        avatar: photoURL,
      });
      setSelectedAvatar(null);
      const getPostsRef = ref(db, "posts");
      const gatheredPosts = await get(getPostsRef);
      gatheredPosts.forEach((post) => {
        if (post.val().userID === localStorage.getItem("currentUserUid")) {
          const getPost = ref(db, `posts/${post.key}`);
          update(getPost, { avatar: photoURL });
        }
        if (post.val().comments.length > 0) {
          post.val().comments.forEach((comment, index) => {
            if (comment.userId === localStorage.getItem("currentUserUid")) {
              const getPost = ref(db, `posts/${post.key}/comments/${index}`);
              update(getPost, { avatar: photoURL });
            }
          });
        }
      });

      hideLoading();
    }
  };

  return (
    <div className="edit-container">
      <h1>Здравей {userData.firstName}</h1>
      <p>Настройки на профила:</p>
      <ul>
        <div>
          <div>Име: {userData.firstName}</div>
          <div>Фамилия: {userData.lastName}</div>
          <div>Email: {userData.email}</div>
          <div>Аватар:</div>
          <img
            className="userAvatar"
            id="avatar"
            src={`${userData.avatar}`}
          ></img>
          <div>
            <div>Смени аватар:</div>
            <input
              type="file"
              onChange={(e) => setSelectedAvatar(e.target.files[0])}
            ></input>
            <button disabled={!selectedAvatar} onClick={handleNewAvatar}>
              Смени
            </button>
          </div>
          <div id="loading"></div>
        </div>
      </ul>
    </div>
  );
};
export default UserProfile;
