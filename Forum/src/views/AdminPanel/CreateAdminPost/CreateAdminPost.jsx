/* eslint-disable no-unused-vars */
import { useState } from "react";
import { auth, db } from "../../../database";
import { ref, push } from "firebase/database"; // Updated import for Realtime Database
import { useNavigate } from "react-router-dom";
import "./CreateAdminPost.css";

const CreateAdminPost = () => {
  const [URL, updateURL] = useState("");
  const [title, updateTitle] = useState("");
  const [tags, updateTags] = useState("");
  const [content, updateContent] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const adminPost = async () => {
    const currUser = auth.currentUser.uid;
    const newPost = {
      title: title,
      imageURL: URL,
      content: content,
      tags: tags.split(" "),
      userID: currUser,
      likes: ["-"],
      postedOn: {
        time: new Date().toLocaleTimeString(),
        date: new Date().toLocaleDateString(),
        fullDate: new Date().getTime(),
      },
    };
    if (title === "" || content === "" || tags === "" || URL === "") {
      return setError("Трябва да попълните всички полета!");
    }
    try {
      const postsRef = ref(db, "adminPosts");
      const newPostRef = push(postsRef, newPost);
      navigate("/");
    } catch {
      setError("Неуспешно създаване на темата");
    }
  };

  const cancelAdminPost = () => {
    navigate("/adminPanel");
  };

  return (
    <>
      <div className="post-container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2">
            {error && <div className="alert">{error}</div>}

            <h1>Създай Админски пост</h1>
            <form action="" method="POST">
              <label htmlFor="title">
                Заглавие <span className="require">*</span>
              </label>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="title"
                  onChange={(e) => updateTitle(e.target.value)}
                />
              </div>
              <label htmlFor="URL">
                Постави Image Adress(URL) <span className="require">*</span>
              </label>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="URL"
                  onChange={(e) => updateURL(e.target.value)}
                />
              </div>
              <label htmlFor="description">
                Съдържание <span className="require">*</span>
              </label>
              <div className="form-group">
                <textarea
                  rows="5"
                  data-expandable
                  onChange={(e) => updateContent(e.target.value)}
                ></textarea>
              </div>
              <label htmlFor="slug">
                Тагове към темата <span className="require">*</span>{" "}
              </label>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control"
                  name="slug"
                  onChange={(e) => updateTags(e.target.value)}
                />
              </div>
              <div className="form-group">
                <p>
                  <span className="require">*</span> - Задължителни полета
                </p>
              </div>
              <div className="form-group">
                <button
                  type="button"
                  className="submit-button"
                  onClick={adminPost}
                >
                  Създай
                </button>
                <button
                  className="cancel-button"
                  type="button"
                  onClick={cancelAdminPost}
                >
                  Откажи
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default CreateAdminPost;
