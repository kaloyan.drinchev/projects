import { ref, update, get } from "firebase/database";
import { db } from "../../database";

export const handleUserSearch = async (
  userInfo,
  selectedValue,
  updateUserData,
  updateFoundStatus,
  errorHandle
) => {
  const usersRef = ref(db, `users`);
  const snapshot = await get(usersRef);
  const filteredUsers = [];

  if (!userInfo) {
    errorHandle("Моля изберете опция");
  }
  if (snapshot.exists()) {
    snapshot.forEach((userSnapshot) => {
      const user = userSnapshot.val();
      if (user[selectedValue] === userInfo) {
        filteredUsers.push(user);
      }
    });

    updateUserData(filteredUsers);
    updateFoundStatus(true);
    if (filteredUsers.length === 0) {
      console.log("FALSE");

      updateFoundStatus(false);
      errorHandle("Няма намерени потребители!");
    }
  }
};

export const handleBlockUser = async (
  userId,
  blockedUsers,
  setBlockedUsers
) => {
  const userRef = ref(db, `users/${userId}`);
  const userSnapshot = await get(userRef);
  const existingData = userSnapshot.val();
  const existingArray = existingData.role || [];
  const newArray = [...existingArray, "blocked"];
  setBlockedUsers((prevBlockedUsers) => new Set([...prevBlockedUsers, userId]));
  await update(userRef, { role: newArray });
};

export const handleUnblockUser = async (
  userId,
  blockedUsers,
  setBlockedUsers
) => {
  const userRef = ref(db, `users/${userId}`);
  const userSnapshot = await get(userRef);
  const existingData = userSnapshot.val();
  const existingArray = existingData.role || [];
  const newArray = existingArray.filter((role) => role !== "blocked");
  setBlockedUsers((prevBlockedUsers) => {
    const newBlockedUsers = new Set(prevBlockedUsers);
    newBlockedUsers.delete(userId);
    return newBlockedUsers;
  });
  await update(userRef, { role: newArray });
};

export const handleSelectChange = (event, setSelectedValue) => {
  const value = event.target.value;
  setSelectedValue(value);
};
