import { useState, useEffect } from "react";
import "./AdminPanel.css";
import {
  handleUserSearch,
  handleBlockUser,
  handleUnblockUser,
  handleSelectChange,
} from "./BlockOrUnblock.js";
import { NavLink } from "react-router-dom";
import { ref, get } from "firebase/database";
import { db } from "../../database";

const AdminPanel = () => {
  const [selectedValue, setSelectedValue] = useState("");
  const [blockedUsers, setBlockedUsers] = useState(new Set());
  const [userInfo, updateUserInfo] = useState("");
  const [isUserFound, updateFoundStatus] = useState(false);
  const [userData, updateUserData] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    const getBlockedUsers = async () => {
      const usersRef = ref(db, `users`);
      const snapshot = await get(usersRef);
      snapshot.forEach((user) => {
        if (user.val().role.includes("blocked")) {
          setBlockedUsers(
            (prevBlockedUsers) =>
              new Set([...prevBlockedUsers, user.val().userID])
          );
        }
      });
    };
    getBlockedUsers();
  }, []);

  return (
    <>
      <div className="container">
        <NavLink to="/adminPost">
          <div
            className="search-post-button"
            style={{ textDecoration: "none !important" }}
          >
            Създай админска тема
          </div>
        </NavLink>
        <div className="searchPage">
          <div className="searchContainer">
            <div>Търси потребител</div>
            <div className="select-field col s12">
              <label className="label">Търсене по:</label>
              <select
                className="select-input"
                value={selectedValue}
                onChange={(e) => handleSelectChange(e, setSelectedValue)}
              >
                <option value="" disabled>
                  Избери опция!
                </option>
                <option value="firstName">Име</option>
                <option value="lastName">Фамилия</option>
                <option value="email">E-mail</option>
                <option value="userID">Потребителско ID</option>
              </select>
            </div>
            <input
              placeholder="Търси..."
              onChange={(e) => updateUserInfo(e.target.value)}
              className="search-input"
            ></input>
            <button
              onClick={() =>
                handleUserSearch(
                  userInfo,
                  selectedValue,
                  updateUserData,
                  updateFoundStatus,
                  setError
                )
              }
              className="search-button"
            >
              Търси
            </button>
          </div>
          <ul className="userResults">
            {isUserFound ? (
              userData.map((element) => {
                const isBlocked = blockedUsers.has(element.userID);
                return (
                  <div key={element.userID} className="user">
                    <div>Име: {element.firstName}</div>
                    <div>Фамилия: {element.lastName}</div>
                    <div>E-mail: {element.email}</div>
                    <div>Статус: {element.role[0]}</div>
                    {isBlocked ? (
                      <button
                        onClick={() =>
                          handleUnblockUser(
                            element.userID,
                            blockedUsers,
                            setBlockedUsers
                          )
                        }
                        className="unblock-button"
                      >
                        Отблокирай
                      </button>
                    ) : (
                      <button
                        onClick={() =>
                          handleBlockUser(
                            element.userID,
                            blockedUsers,
                            setBlockedUsers
                          )
                        }
                        className="block-button"
                      >
                        Блокирай
                      </button>
                    )}
                  </div>
                );
              })
            ) : (
              <div id="alert">{error}</div>
            )}
          </ul>
        </div>
      </div>
    </>
  );
};

export default AdminPanel;
