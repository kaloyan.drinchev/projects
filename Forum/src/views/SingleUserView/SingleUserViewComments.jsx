import { useLocation, NavLink } from "react-router-dom";
import { get, ref } from "firebase/database";
import { useState, useEffect } from "react";
import { db } from "../../database";
import "./SingleUserViewComments.css";

const SingleUserViewComments = () => {
  const [userData, setUserData] = useState({});
  const [getPosts, updatePostData] = useState([]);
  const [postsMissing, setMissingStatus] = useState(false);
  const location = useLocation();
  const userID = location.state;

  useEffect(() => {
    const fetchUserData = async () => {
      const getRef = ref(db, `users/${userID}`);
      const gatheredUser = await get(getRef);
      const getUser = gatheredUser.val();
      setUserData(getUser);

      if (getUser) {
        const postsRef = ref(db, `posts`);
        const snapshot = await get(postsRef);
        const filteredPosts = [];

        if (snapshot.exists()) {
          snapshot.forEach((postSnapshot) => {
            const post = postSnapshot.val();
            post.key = postSnapshot.key;
            if (post.userID === userID) {
              filteredPosts.push(post);
            }
          });
          updatePostData(filteredPosts);
        } else {
          setMissingStatus(true);
        }
      }
    };

    fetchUserData();
  }, [userID]);

  return (
    <div>
      {userData && Object.keys(userData).length > 0 && (
        <div id="user-info-container">
          <div>
            <div>
              {userData.firstName} {userData.lastName}
            </div>
            <img src={userData.avatar} alt="User Avatar" />
          </div>
          <div>
            Статус: {userData.role.includes("blocked") ? "Блокиран" : "Активен"}
          </div>
          <div>
            Правомощия:{" "}
            {userData.role.includes("user") ? "Потребител" : "Администратор"}
          </div>
          <div>Дата на регистрация: {userData.creationDate}</div>

            <div>
              <h3>Теми:</h3>
            </div>
          <div id="userPostsCollection">
            {!postsMissing ? (
              getPosts.map((element) => (
                <article className="post" key={element.key}>
                  <div className="postContainer">
                    <div id="title">
                      <NavLink
                        to={`/posts/${element.key}`}
                        state={element}
                        className="title"
                      >
                        {element.title}
                      </NavLink>
                      <div className="postOwnerDetails">
                        <div>
                          Публикувано от:{" "}
                          <img
                            src={userData.avatar}
                            alt="User Avatar"
                            className="ViewUserPostAvatar"
                          />{" "}
                          {userData.firstName} {userData.lastName}
                        </div>
                      </div>
                      <div>
                        Публикувано на: {element.postedOn.date}{" "}
                        {element.postedOn.time}
                      </div>
                      <div className="content">{element.content}</div>
                      <div className="tags">
                        Тагове: {element.tags.join(", ")}
                      </div>
                      <div className="likes-comments">
                        <span className="icon">
                          👍 {element.likes.length - 1}
                        </span>
                        <span className="icon">
                          💬 {element.comments.length - 1}
                        </span>
                      </div>
                    </div>
                  </div>
                </article>
              ))
            ) : (
              <div>Този потребител няма постове до момента.</div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default SingleUserViewComments;
