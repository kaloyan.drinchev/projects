import "./Weather.css";
import { useEffect, useState } from "react";
import { API_KEY } from "../../common/constants";
import {
  // latitudeBurgas,
  // longitudeBurgas,
  latitudeRhodope,
  longitudeRhodope,
  latitudeVarna,
  longitudeVarna,
  latitudeVitosha,
  longitudeVitosha,
  // longitudePlovdiv,
  // latitudePlovdiv,
  longitudePirin,
  latitudePirin,
  longitudeRila,
  latitudeRila,
  longitudeStaraPlanina,
  latitudeStaraPlanina,
} from "../../common/constants";
import Stats from "./Stats";
import slideIcon from "../../assets/GreenButton_RightArrow.svg";
export default function Weather() {
  // const [sofiaWeather, setSofiaWeather] = useState("");
  const [vitoshaWeather, setVitoshaWeather] = useState("");
  const [varnaWeather, setVarnaWeather] = useState("");
  // const [burgasWeather, setBurgasWeather] = useState("");
  // const [plovdivWeather, setPlovdivWeather] = useState("");
  const [rhodopeWeather, setRhodopeWeather] = useState("");
  const [pirinWeather, setPirinWeather] = useState("");
  const [rilaWeather, setRilaWeather] = useState("");
  const [staraPlaninaWeather, setStaraPlaninaWeather] = useState("");

  const handleSlideLeft = () => {
    const container = document.querySelector(".weather-data-container");
    container.scrollBy({ left: -400, behavior: "smooth" });
  };

  const handleSlideRight = () => {
    const container = document.querySelector(".weather-data-container");
    container.scrollBy({ left: 360, behavior: "smooth" });
  };
  useEffect(() => {
    // fetch(`http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=Sofia`)
    //   .then((response) => response.json())
    //   .then((data) => {
    //     setSofiaWeather(data.current);
    //   });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeVitosha},${longitudeVitosha}`
    )
      .then((response) => response.json())
      .then((data) => {
        setVitoshaWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Vitosha weather data:", error);
      });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeRila},${longitudeRila}`
    )
      .then((response) => response.json())
      .then((data) => {
        setRilaWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Vitosha weather data:", error);
      });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeRila},${longitudeRila}`
    )
      .then((response) => response.json())
      .then((data) => {
        setRilaWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Rila Mountain weather data:", error);
      });
    // fetch(
    //   `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudePlovdiv},${longitudePlovdiv}`
    // )
    //   .then((response) => response.json())
    //   .then((data) => {
    //     setPlovdivWeather(data.current);
    //   })
    //   .catch((error) => {
    //     console.error("Error fetching Plovdiv weather data:", error);
    //   });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeRhodope},${longitudeRhodope}`
    )
      .then((response) => response.json())
      .then((data) => {
        setRhodopeWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Rhodope weather data:", error);
      });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudePirin},${longitudePirin}`
    )
      .then((response) => response.json())
      .then((data) => {
        setPirinWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Pirin Mountain weather data:", error);
      });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeVarna},${longitudeVarna}`
    )
      .then((response) => response.json())
      .then((data) => {
        setVarnaWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Carna weather data:", error);
      });
    fetch(
      `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeStaraPlanina},${longitudeStaraPlanina}`
    )
      .then((response) => response.json())
      .then((data) => {
        setStaraPlaninaWeather(data.current);
      })
      .catch((error) => {
        console.error("Error fetching Carna weather data:", error);
      });
    // fetch(
    //   `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${latitudeBurgas},${longitudeBurgas}`
    // )
    //   .then((response) => response.json())
    //   .then((data) => {
    //     setBurgasWeather(data.current);
    //   })
    //   .catch((error) => {
    //     console.error("Error fetching Burgas weather data:", error);
    //   });
  }, []);

  return (
    <>
      <div className="side-button">
        <img
          className="slideLeft"
          src={slideIcon}
          onClick={handleSlideLeft}
        ></img>
      </div>

      <div className="container-scroll">
        <div className="weather-container">
          <h3 className="weather-title">Времето в момента</h3>
          <div className="horizontal-scroll-container">
            <div className="weather-data-container">
              {/* <Stats
                description="София"
                count={sofiaWeather.temp_c + "°C"}
                icon={sofiaWeather?.condition?.icon}
              />
              <Stats
                description="Бургас"
                count={burgasWeather.temp_c + "°C"}
                icon={burgasWeather?.condition?.icon}
              /> */}
              <Stats
                description="Витоша"
                count={vitoshaWeather.temp_c + "°C"}
                icon={vitoshaWeather?.condition?.icon}
              />
              <Stats
                description="Родопи"
                count={rhodopeWeather.temp_c + "°C"}
                icon={rhodopeWeather?.condition?.icon}
              />
              <Stats
                description="Рила"
                count={rilaWeather.temp_c + "°C"}
                icon={rilaWeather?.condition?.icon}
              />
              <Stats
                description="Пирин"
                count={pirinWeather.temp_c + "°C"}
                icon={pirinWeather?.condition?.icon}
              />
              <Stats
                description="Стара планина"
                count={staraPlaninaWeather.temp_c + "°C"}
                icon={staraPlaninaWeather?.condition?.icon}
              />
              <Stats
                description="Варна"
                count={varnaWeather.temp_c + "°C"}
                icon={varnaWeather?.condition?.icon}
              />
              {/* <Stats
                description="Пловдив"
                count={plovdivWeather.temp_c + "°C"}
                icon={plovdivWeather?.condition?.icon}
              /> */}
            </div>
          </div>
        </div>
      </div>
      <div className="side-button">
        <img
          className="slideRight"
          src={slideIcon}
          onClick={handleSlideRight}
        ></img>
      </div>
    </>
  );
}
