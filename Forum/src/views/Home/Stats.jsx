/* eslint-disable react/prop-types */
import "./Stats.css";

export default function Stats({ description, count, icon }) {
  return (
    <div className="stats">
      {!!icon && <img src={icon} className="stats-icon" />}
      <p className="stats-description">{description}</p>
      <p className="stats-count">{count}</p>
    </div>
  );
}
