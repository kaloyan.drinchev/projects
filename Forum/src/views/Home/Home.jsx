import LoadAdminPosts from "../../components/LoadAdminPosts/LoadAdminPosts";
import "./Home.css";
import { useEffect, useState } from "react";
import { ref, onValue } from "firebase/database";
import { db } from "../../database";
import Stats from "./Stats";
import Weather from "./Weather";

const Home = () => {
  const [userCount, setUserCount] = useState(0);
  const [postsCount, setPostsCount] = useState(0);

  useEffect(() => {
    const usersRef = ref(db, "users");
    const postsRef = ref(db, "posts");

    onValue(usersRef, (snapshot) => {
      if (snapshot.exists()) {
        setUserCount(Object.keys(snapshot.val()).length);
      }
    });

    onValue(postsRef, (snapshot) => {
      if (snapshot.exists()) {
        setPostsCount(Object.keys(snapshot.val()).length);
      }
    });
  }, []);

  return (
    <div>
      <div className="info-container">
        <Weather />
      </div>
      <div className="info-container">
        <Stats description="Регистрации" count={userCount} />
        <Stats description="Теми" count={postsCount} />
      </div>
      <div className="adminPostsLoadout">
        <h1 className="postsHeader">Топ 10 места избрани от авторите:</h1>
        <LoadAdminPosts></LoadAdminPosts>
      </div>
    </div>
  );
};
export default Home;
