import { db } from "../../database";
import { ref, get } from "firebase/database";
import { useState, useEffect } from "react";
import "./YourPosts.css";
import { NavLink } from "react-router-dom";
import { useUserContext } from "../../context";
import Stats from "../Home/Stats";

const YourPosts = () => {
  const [userPostData, updatePostData] = useState([]);
  const { userData } = useUserContext();
  const [userLikedPosts, setUserLikedPosts] = useState([]);

  useEffect(() => {
    const fetchUserPosts = async () => {
      const postsRef = ref(db, `posts`);
      const snapshot = await get(postsRef);
      const filteredPosts = [];
      const filteredLikedPosts = [];

      if (snapshot.exists()) {
        snapshot.forEach((postSnapshot) => {
          const post = postSnapshot.val();
          post.key = postSnapshot.key;
          if (post.userID === localStorage.getItem("currentUserUid")) {
            filteredPosts.push(post);
          }
        });
        updatePostData(filteredPosts);
      }
      if (snapshot.exists()) {
        snapshot.forEach((postSnapshot) => {
          const post = postSnapshot.val();
          post.key = postSnapshot.key;
          if (post.likes.includes(localStorage.getItem("currentUserUid"))) {
            filteredLikedPosts.push(post);
          }
        });
        setUserLikedPosts(filteredLikedPosts);
      }
    };
    fetchUserPosts();
  }, []);
  return (
    <>
      <div className="user-liked-and-own-posts">
        <div className="user-own-posts">
          <Stats description="Твоите теми" count={userPostData.length} />
          {Array.isArray(userPostData) &&
            userPostData.map((element) => {
              return (
                <article className="post" key={element.key}>
                  <div className="postContainer">
                    <div id="title">
                      <NavLink
                        to={`/posts/${element.key}`}
                        state={element}
                        className="title"
                      >
                        {element.title}
                      </NavLink>
                      <div className="postOwnerDetails">
                        <div>
                          <img
                            id="your-posts-avatar"
                            src={userData.avatar}
                            className="ViewUserPostAvatar"
                          ></img>{" "}
                          {userData.firstName} {userData.lastName}
                        </div>
                      </div>
                      <div>
                        {element.postedOn.date} {element.postedOn.time}
                      </div>
                      <div className="content">{element.content}</div>
                      <div className="tags">
                        Тагове: {element.tags.join(", ")}
                      </div>
                      <div className="likes-comments">
                        <span className="icon">
                          👍 {element.likes.length - 1}
                        </span>
                        <span className="icon">
                          💬 {element.comments.length - 1}
                        </span>
                      </div>
                    </div>
                  </div>
                </article>
              );
            })}
        </div>
        <div className="user-liked-posts">
          <Stats
            description="Твоите харесани теми"
            count={userLikedPosts.length}
          />

          {Array.isArray(userLikedPosts) &&
            userLikedPosts.map((element) => {
              return (
                <article className="post" key={element.key}>
                  <div className="postContainer">
                    <div id="title">
                      <NavLink
                        to={`/posts/${element.key}`}
                        state={element}
                        className="title"
                      >
                        {element.title}
                      </NavLink>
                      <div className="postOwnerDetails">
                        <div>
                          <img
                            id="your-posts-avatar"
                            src={element.avatar}
                            className="ViewUserPostAvatar"
                          ></img>{" "}
                          {element.owner}
                        </div>
                      </div>
                      <div>
                        {element.postedOn.date} {element.postedOn.time}
                      </div>
                      <div className="content">{element.content}</div>
                      <div className="tags">
                        Тагове: {element.tags.join(", ")}
                      </div>
                      <div className="likes-comments">
                        <span className="icon">
                          👍 {element.likes.length - 1}
                        </span>
                        <span className="icon">
                          💬 {element.comments.length - 1}
                        </span>
                      </div>
                    </div>
                  </div>
                </article>
              );
            })}
        </div>
      </div>
    </>
  );
};

export default YourPosts;
