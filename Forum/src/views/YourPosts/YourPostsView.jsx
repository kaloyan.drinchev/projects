/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import { useLocation, useNavigate, NavLink } from "react-router-dom";
import { get, ref, update, remove } from "firebase/database";
import { db } from "../../database";
import "./YourPostsView.css";
import Linkify from "react-linkify";
import {
  MAX_LENGTH_CONTENT,
  MAX_LENGTH_TITLE,
  MIN_LENGTH_CONTENT,
  MIN_LENGTH_TITLE,
} from "../../common/constants.js";
export default function YourPostsView() {
  const [commentBoxValue, setCommentBoxValue] = useState("");
  const [userComments, setUserComments] = useState([]);
  const [isLiked, setLikeStatus] = useState(false);
  const [editComment, setEditComment] = useState("");
  const [editTitle, setEditTitle] = useState("");
  const [isEditTitleClicked, setEditTitleStatus] = useState(false);
  const [isEditContentClicked, setEditContentStatus] = useState(false);
  const location = useLocation();
  const articleID = location.state;
  const [postData, setPostData] = useState(articleID);
  const [editContent, setEditContent] = useState("");
  const [editedComments, setEditedComments] = useState([]);
  const [doesUserOwnPost, setUserOwnPost] = useState(false);
  const [editTags, setEditTags] = useState([]);
  const [isEditTagsClicked, setEditTagsStatus] = useState(false);
  const [titleTooShortLong, setTitleTooShortLong] = useState(false);
  const [contentTooShortLong, setContentTooShortLong] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const userRef = ref(db, `posts/${articleID.key}`);
      const userSnapshot = await get(userRef);
      const updatedData = userSnapshot.val();

      setPostData(updatedData);
      setEditContent(updatedData.content);
      setEditTitle(updatedData.title);
      setEditTags([...updatedData.tags]);
      const comment = updatedData.comments;
      const commentDestructuring = [...comment];
      commentDestructuring.shift();
      const initialComments = commentDestructuring.map((comment) => ({
        ...comment,
        isEditing: false,
      }));
      setUserComments(initialComments);
      if (updatedData.likes.includes(localStorage.getItem("currentUserUid"))) {
        setLikeStatus(true);
      }
    };

    if (
      localStorage.getItem("currentUserUid") === articleID.userID ||
      localStorage.getItem("isAdminLogged")
    ) {
      setUserOwnPost(true);
    }

    fetchData();
  }, [articleID.key]);

  const editStatus = (index) => {
    const updatedComments = [...userComments];
    setEditComment(updatedComments[index].content);
    updatedComments[index].isEditing = !updatedComments[index].isEditing;
    setUserComments(updatedComments);
  };

  const handleCommentDelete = async (index) => {
    const updatedComments = [...userComments];

    const shouldDelete = window.confirm(
      "Сигурни ли сте, че искате да изтриете този коментар?"
    );
    if (shouldDelete) {
      const userRef = ref(db, `posts/${articleID.key}`);
      const userSnapshot = await get(userRef);
      const existingData = userSnapshot.val();
      const existingArray = existingData.comments || [];
      existingArray.splice(index + 1, 1);
      await update(userRef, { comments: existingArray });
      updatedComments.splice(index, 1);
      setUserComments(updatedComments);
    }
  };
  const handlePostDelete = async () => {
    const shouldDelete = window.confirm(
      "Сигурни ли сте, че искате да изтриете този пост?"
    );
    if (shouldDelete) {
      const userRef = ref(db, `posts/${articleID.key}`);
      await remove(userRef);
      navigate("/yourPosts");
    }
  };
  const handlePostLike = async () => {
    if (!localStorage.getItem("loginStatus")) {
      navigate("/login");
      alert("Трябва да влезеш в профила си за да хересаш тази публикация!");
    } else {
      const getPost = ref(db, `posts/${articleID.key}`);
      const gatheredPost = await get(getPost);
      if (
        !gatheredPost
          .val()
          .likes.includes(localStorage.getItem("currentUserUid"))
      ) {
        const updatePost = gatheredPost.val().likes;
        const newPost = [...updatePost, localStorage.getItem("currentUserUid")];
        await update(getPost, { likes: newPost });
        setLikeStatus(!isLiked);
      }
    }
  };

  const handlePostUnlike = async () => {
    if (!localStorage.getItem("loginStatus")) {
      navigate("/login");
      alert("Трябва да влезеш в профила си за да хересаш тази публикация!");
    } else {
      const getPost = ref(db, `posts/${articleID.key}`);
      const gatheredPost = await get(getPost);
      const updatePost = gatheredPost.val().likes;
      const newPost = updatePost.filter(
        (like) => like !== localStorage.getItem("currentUserUid")
      );
      await update(getPost, { likes: newPost });
      setLikeStatus(!isLiked);
    }
  };

  const handlePostClick = async () => {
    if (!localStorage.getItem("loginStatus")) {
      navigate("/login");
      alert("Трябва да влезеш в профила си за да коментираш тази публикация!");
    } else {
      const userName = ref(
        db,
        `users/${localStorage.getItem("currentUserUid")}`
      );
      const myName = await get(userName);
      const myFullName = myName.val();
      if (myFullName.role.includes("blocked")) {
        alert("Блокиран си батка.");
      } else {
        if (commentBoxValue.trim() !== "") {
          const newComment = {
            content: commentBoxValue,
            fullName: `${myFullName.firstName} ${myFullName.lastName}`,
            avatar: myFullName.avatar,
            userId: localStorage.getItem("currentUserUid"),
            postedOn: {
              time: new Date().toLocaleTimeString(),
              date: new Date().toLocaleDateString(),
              fullDate: new Date().getTime(),
            },
          };

          const userRef = ref(db, `posts/${articleID.key}`);
          const userSnapshot = await get(userRef);
          const existingData = userSnapshot.val();
          const existingArray = existingData.comments || [];
          const newArray = [...existingArray, newComment];
          setUserComments([...userComments, newComment]);
          await update(userRef, { comments: newArray });
          setCommentBoxValue("");
        }
      }
    }
  };

  const handleCommentChange = (event) => {
    const newComment = event.target.value;
    setEditComment(newComment);
  };

  const handleInputChange = (event) => {
    setCommentBoxValue(event.target.value);
  };
  const handleTitleInput = (event) => {
    setEditTitle(event.target.value);
  };

  const handleTagsInput = (event) => {
    setEditTags(event.target.value.split(" "));
  };
  const handleCommentEdit = async (index, editedComment) => {
    const updatedComments = [...userComments];
    updatedComments[index].content = editedComment;
    updatedComments[index].isEditing = false;
    setUserComments(updatedComments);
    setEditedComments((prevEdited) => ({
      ...prevEdited,
      [index]: { ...updatedComments[index], content: editComment },
    }));

    const userRef = ref(db, `posts/${articleID.key}`);
    const userSnapshot = await get(userRef);
    const existingData = userSnapshot.val();
    const existingArray = existingData.comments || [];
    existingArray[index + 1] = updatedComments[index];
    await update(userRef, { comments: existingArray });

    setEditComment("");
  };

  const handleTitleEditClick = () => {
    setEditTitleStatus(!isEditTitleClicked);
  };
  const handleTagsEditClick = () => {
    setEditTagsStatus(!isEditTagsClicked);
  };
  const handleContentEditClick = () => {
    setEditContentStatus(!isEditContentClicked);
  };

  const handleTitleChange = async () => {
    if (
      editTitle.length < MIN_LENGTH_TITLE ||
      editTitle.length > MAX_LENGTH_TITLE
    ) {
      setTitleTooShortLong(true);
    } else {
      console.log(editTitle);
      console.log(editTitle.length);
      setTitleTooShortLong(false);
      const userRef = ref(db, `posts/${articleID.key}`);
      const postOwner = await get(userRef);
      await update(userRef, { title: editTitle });
      setEditTitleStatus(false);
      setPostData((prevData) => ({ ...prevData, title: editTitle }));
    }
  };

  const handleContentChange = (event) => {
    const newContent = event.target.value;
    setEditContent(newContent);
  };

  const handleTagsChange = async () => {
    const userRef = ref(db, `posts/${articleID.key}`);
    const postOwner = await get(userRef);

    await update(userRef, { tags: editTags.join(",").split(",") });
    setEditTagsStatus(false);
    setPostData((prevData) => ({ ...prevData, tags: editTags }));
  };

  const handleContentEdit = async () => {
    if (
      editContent.length < MIN_LENGTH_CONTENT ||
      editContent.length > MAX_LENGTH_CONTENT
    ) {
      setContentTooShortLong(true);
    } else {
      setContentTooShortLong(false);
      const userRef = ref(db, `posts/${articleID.key}`);
      const userSnapshot = await get(userRef);
      const existingData = userSnapshot.val();

      const updatedPost = { ...existingData, content: editContent };

      await update(userRef, updatedPost);
      setEditContentStatus(false);
      setPostData((prevData) => ({ ...prevData, content: editContent }));
    }
  };
  return (
    <div className="article-user-single-post-view">
      <div>
        {!isLiked ? (
          <button className="likeButton" onClick={handlePostLike}>
            Харесай
          </button>
        ) : (
          <button className="unlikeButton" onClick={handlePostUnlike}>
            Харесано!
          </button>
        )}
      </div>
      <div className="single-post-view">
        {!isEditTitleClicked ? (
          <div className="post-title-user-single-post-view">
            <h1 id="single-post-title">{postData.title}</h1>
            <div className="btn-container">
              {doesUserOwnPost && (
                <button
                  type="button"
                  className="edit-title-button"
                  onClick={handleTitleEditClick}
                >
                  Редактирай
                </button>
              )}
              {doesUserOwnPost && (
                <button
                  type="button"
                  className="edit-title-button"
                  onClick={() => handlePostDelete()}
                >
                  Изтрий темата
                </button>
              )}
            </div>
          </div>
        ) : (
          <>
            <input
              type="text"
              value={editTitle}
              onChange={handleTitleInput}
              className="edit-title-input"
            />
            <button type="button" onClick={handleTitleChange}>
              Запази
            </button>
            <button type="button" onClick={handleTitleEditClick}>
              Откажи
            </button>
            {titleTooShortLong && (
              <div id="errorTooShortLong">
                Заглавието на темата трябва да е между 16 и 32 символа!
              </div>
            )}
          </>
        )}
        <div>
          <NavLink to={`user/${postData.userID}`} state={postData.userID}>
            <img src={postData.avatar} className="commentsAvatar"></img>{" "}
            {postData.owner}
          </NavLink>
          <div id="date-time">
            {postData.postedOn.date} {postData.postedOn.time}
          </div>
        </div>
        {!isEditContentClicked ? (
          <div className="content-user-single-post-view">
            <p>
              <Linkify>{postData.content}</Linkify>
            </p>
            {doesUserOwnPost && (
              <button
                type="button"
                className="edit-content-button"
                onClick={handleContentEditClick}
              >
                Редактирай
              </button>
            )}
          </div>
        ) : (
          <>
            <textarea
              type="text"
              value={editContent}
              onChange={handleContentChange}
              className="edit-content-input"
            />
            <button type="button" onClick={handleContentEdit}>
              Запази
            </button>
            <button type="button" onClick={handleContentEditClick}>
              Откажи
            </button>
            {contentTooShortLong && (
              <div id="errorTooShortLong">
                Съдържанието на темата трябва да е между 32 и 8192 символа!
              </div>
            )}
          </>
        )}
        {!isEditTagsClicked ? (
          <div className="post-tags-user-single-post-view">
            <h4 id="tags-input-value">Тагове: {postData.tags.join(", ")}</h4>
            <div className="btn-container">
              {doesUserOwnPost && (
                <button
                  type="button"
                  value={postData.tags.join(", ")}
                  className="edit-tags-button"
                  onClick={handleTagsEditClick}
                >
                  Редактирай
                </button>
              )}
            </div>
          </div>
        ) : (
          <>
            <input
              type="text"
              value={editTags}
              onChange={handleTagsInput}
              className="edit-tags-input"
            />
            <button type="button" onClick={handleTagsChange}>
              Запази
            </button>
            <button type="button" onClick={handleTagsEditClick}>
              Откажи
            </button>
          </>
        )}
        <div className="comments-container">
          <input
            type="text"
            placeholder="Напиши коментар..."
            value={commentBoxValue}
            onChange={handleInputChange}
            className="comment-input"
          />
          <button className="post-button" onClick={handlePostClick}>
            Създай
          </button>
          <div id="comments-head">Коментари</div>
          <ul id="unordered">
            {userComments.length > 0 ? (
              userComments.map((comment, index) => (
                <div key={index} className="single-comments">
                  <div className="comment-info">
                    <div className="SingleUserComment">
                      <NavLink
                        to={`user/${comment.userId}`}
                        state={comment.userId}
                      >
                        <img
                          src={comment.avatar}
                          className="commentsAvatar"
                        ></img>{" "}
                        {comment.fullName}
                      </NavLink>
                      <div>
                        {comment.postedOn.date} {comment.postedOn.time}
                      </div>{" "}
                    </div>
                    {comment.userId ===
                      localStorage.getItem("currentUserUid") ||
                    localStorage.getItem("isAdminLogged") ? (
                      <span>
                        <button
                          type="button"
                          className="edit-comment-button"
                          onClick={() => editStatus(index)}
                        >
                          Редактирай
                        </button>{" "}
                        <span>
                          {" "}
                          <button
                            onClick={() => {
                              handleCommentDelete(index);
                            }}
                          >
                            Изтрий
                          </button>
                        </span>
                      </span>
                    ) : (
                      <div></div>
                    )}
                  </div>

                  {!comment.isEditing ? (
                    <div className="comment-content">
                      <Linkify>{comment.content}</Linkify>
                    </div>
                  ) : (
                    <>
                      <input
                        className="comment-content"
                        type="text"
                        value={editComment}
                        onChange={handleCommentChange}
                      />

                      <button
                        type="button"
                        onClick={() => handleCommentEdit(index, editComment)}
                      >
                        Запази
                      </button>
                    </>
                  )}
                </div>
              ))
            ) : (
              <div>Все още няма коментари</div>
            )}
          </ul>
        </div>
      </div>
    </div>
  );
}
