 import { auth } from '../../database'
import { signOut } from 'firebase/auth';
import "./SignOut.css"


   export const handleSignOut = () => {
       signOut(auth).then(() => { // log-outva currently lognatiq user
            localStorage.clear() // zachistva ima li lognat user ili ne
            window.location.pathname = "/login" // redirektva na login page-a
        }).catch((error) => {
            alert(error)
        });
    }
    
      
