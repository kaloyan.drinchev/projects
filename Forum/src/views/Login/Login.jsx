import { useState } from "react";
import { auth, db } from "../../database";
import { signInWithEmailAndPassword } from "firebase/auth";
import { NavLink } from "react-router-dom";
import { ref, get } from "firebase/database";
import "./Login.css";

const Login = () => {
  const [password, updatePassword] = useState("");
  const [email, updateEmail] = useState("");
  const [error, setError] = useState("");
  const [isLogged, setIsLogged] = useState(false);

  const handleLogin = async () => {
    try {
      const signUserIn = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );

      const userRef = ref(db, `users/${signUserIn.user.uid}`);
      const userSnapshot = await get(userRef);
      if (userSnapshot.exists()) {
        const userData = userSnapshot.val();
        if (userData.role && userData.role.includes("admin")) {
          localStorage.setItem("isAdminLogged", true);
        }
      }
      setIsLogged(true);
      localStorage.setItem("loginStatus", true);
      localStorage.setItem("currentUserUid", auth.currentUser.uid);
      window.location.pathname = "/";
    } catch (error) {
      setError("Грешен е-майл или парола");
      console.error(error);
    }
  };

  return (
    <div className="loginPage">
      <div className="loginContainer">
        {!isLogged && <div id="alert">{error}</div>}
        <p>Вход с E-mail и Парола</p>
        <div>
          <div>E-mail:</div>
          <input
            type="email"
            placeholder="E-mail..."
            value={email}
            onChange={(e) => updateEmail(e.target.value)}
          ></input>
        </div>
        <div>
          <div>Парола:</div>
          <input
            type="password"
            placeholder="Password..."
            value={password}
            onChange={(e) => updatePassword(e.target.value)}
          ></input>
        </div>
        <button onClick={handleLogin}>Вход</button>
        <div className="signupText">
          <NavLink to="/forgot-password">Забравена парола?</NavLink>
        </div>
        <div className="signupText">
          Нямаш регистрация? <NavLink to="/signUp"> Регистрация </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Login;
