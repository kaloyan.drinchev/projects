import { useState } from "react";
import "./SignUp.css";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { ref, set } from "firebase/database";
import { auth, db } from "../../database";
import { NavLink } from "react-router-dom";

const MIN_LENGTH = 4;
const MAX_LENGTH = 32;

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [error, setError] = useState("");
  const [isRegistered, setIsRegistered] = useState(false);

  const handleSignUp = async () => {
    try {
      const newUser = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );

      const userData = {
        userID: newUser.user.uid,
        avatar:
          "https://storage.needpix.com/rsynced_images/question-158453_1280.png",
        firstName: firstName,
        lastName: lastName,
        email: email,
        role: ["user"],
        creationDate: new Date().toDateString(),
      };
      const userRef = ref(db, "users/" + newUser.user.uid);
      await set(userRef, userData);
      setIsRegistered(true);
      localStorage.setItem("loginStatus", true);
      localStorage.setItem("currentUserUid", newUser.user.uid);
      window.location.pathname = "/";

      if (password === "") setError("Моля, въведете парола");
      if (firstName === "") setError("Моля, въведете първото си име");
      if (lastName === "") setError("Моля, въведете фамилното си име");
      if (firstName.length < MIN_LENGTH || firstName.length > MAX_LENGTH) {
        setError("Първото ви име трябва да е между 4 и 32 символа");
      }
      if (lastName.length < MIN_LENGTH || lastName.length > MAX_LENGTH) {
        setError("Първото ви име трябва да е между 4 и 32 символа");
      }
      if (
        firstName === "" &&
        lastName === "" &&
        password === "" &&
        email === ""
      ) {
        setError("Моля, попълнете всички полета!");
      }
    } catch (e) {
      if (email === "") {
        setError("Моля, въведете своя имейл");
      } else setError("Този е-майл адрес се използва!");
    }
  };

  return (
    <div className="signUpPage">
      <div className="signUpContainer">
        {!isRegistered && <div id="alert">{error}</div>}
        <div>
          <div>Твоят e-mail:</div>
          <input
            type="email"
            placeholder="e-mail..."
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div>
          <div>Избери парола:</div>
          <input
            type="password"
            placeholder="Парола..."
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div>
          <div>Име:</div>
          <input
            type="text"
            placeholder="Име..."
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </div>
        <div>
          <div>Фамилия:</div>
          <input
            type="text"
            placeholder="Фамилия..."
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </div>
        <div>
          <button onClick={handleSignUp} className="signUpContainer-button">
            Потвърди
          </button>
        </div>
        <div>
          Вече имаш регистрация? <NavLink to="/login"> Вход </NavLink>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
