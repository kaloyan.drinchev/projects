export const API_KEY = "bbb1ad3d5116404ba98115924231108";

// Cherni vrah
export const latitudeVitosha = 42.65789;
export const longitudeVitosha = 23.33206;

//Rodopite
export const latitudeRhodope = 41.65195;
export const longitudeRhodope = 24.68748;

// Varna
export const latitudeVarna = 42.66523;
export const longitudeVarna = 27.24396;

//Burgas
export const latitudeBurgas = 42.50628;
export const longitudeBurgas = 27.46263;

//Plovdiv
export const latitudePlovdiv = 42.136097;
export const longitudePlovdiv = 24.742168;

//Pirin
export const latitudePirin = 42.13333;
export const longitudePirin = 23.13333;

//Rila
export const latitudeRila = 41.7583303;
export const longitudeRila = 23.42166498;

//Stara planina
export const latitudeStaraPlanina = 43.249999;
export const longitudeStaraPlanina = 25.0;

export const MIN_LENGTH_TITLE = 16;
export const MAX_LENGTH_TITLE = 64;
export const MIN_LENGTH_CONTENT = 32;
export const MAX_LENGTH_CONTENT = 8192;
