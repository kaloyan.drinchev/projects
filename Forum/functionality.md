## Functionality of the page

### home page buttons

- `NavBar` always on top of all pages in our forum!

- `HOME` button - will navigate you to the home page ` '/'` ,
  `'/home' `

- `LOG-IN` button - email address and password
  Need an account ? `SIGN-UP` button `<NavLink to='signup'/>`

- `SIGN-UP` button - email address, user_name, password and conformation password
  Already have an account? `LOG-IN` button `<NavLink to='login'/>`

- `SEARCH BAR` button - only available if user is logged in
  if not -navigates you to `Log in` page - `path='/login'`

  `CLICK ON POST` button - clears the content and shows us the detailed post - author, title, content, likes, dislikes, comments count. WHEN NOT LOGGED IN - you aren't able to comment like and dislike the post. IF YOU CLICK LIKE/COMMENT/DISLIKE buttons -
  WE REDIRECT YOU TO `Log in` page - `path='/login'`

### logged in page buttons

- `SEARCH BAR` button - search posts with particular tags
  if admin - consult with Trainer on wednesday 15;00

- `CREATE POST ` button - clears the current page and redirect us
  to `<Link to='/create-post'/>` component which will have title and context fields and a `submit post` button

`CLICK ON POST` button - clears the content and shows us the detailed post - author, title, content, likes, dislikes, comments count. WHEN LOGGED IN - you are able to comment like and dislike
IF OWNER OF THE POST - able to edit the context and the title.
n
