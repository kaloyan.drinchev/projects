import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";

// import NavBar from "./components/NavBar/NavBar";
import Body from "./components/Body/Body";
// import SideBar from "./components/SideBar/SideBar";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Body />}>
      {/* <Route index element={<Dashboard />} />
      <Route path="create" element={<Create />} action={createAction} />
      <Route path="profile" element={<Profile />} /> */}
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
