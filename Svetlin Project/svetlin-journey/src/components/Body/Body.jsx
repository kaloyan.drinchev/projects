// import { useState, useEffect } from "react";
import { Button, Card, Col, Container } from "react-bootstrap";
import Row from "react-bootstrap/Row";
export default function Body() {
  const products = [
    {
      id: 1,
      title: "1-25 WORKOUTS",
      description: "Every big journey starts with one step.",
      author: "Svetlin",
      //   img: "/img/mario.png",
      price: "11 BGN (лв)",
    },
    {
      id: 2,
      title: "26-50 WORKOUTS",
      description: "Experience the next level of your calisthenic growth!",
      author: "Svetlin",
      //   img: "/img/yoshi.png",
      price: "11 BGN (лв)",
    },
    {
      id: 3,
      title: "51-75 WORKOUTS",
      description:
        "Unlock your hidden potential and set forth on a transformative journey",
      author: "Peach",
      //   img: "/img/yoshi.png",
      price: "11 BGN (лв)",
    },
  ];
  //   "../../constants/img/sv1-25.png"
  return (
    <>
      <Container>
        <Row xs={1} md={12}>
          {products.map((product) => (
            <Col md={4} key={product.id}>
              <Card>
                <Card.Img
                  src="https://svetlinsjourney.com/cdn/shop/files/image_8a617f20-267d-4217-8bd3-9eaf444fe9ea.png?v=1692141648&width=713/"
                  style={{ width: "25%", height: "auto" }}
                />
                <Card.Body>
                  <Card.Title>{product.title}</Card.Title>
                  <Card.Text>{product.description}</Card.Text>
                  <Card.Text>{product.price}</Card.Text>
                  <Button variant="primary">Add to Card</Button>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}
