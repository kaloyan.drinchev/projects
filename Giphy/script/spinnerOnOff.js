/**
 * Shows the spinner when a fetch request is made
 */
const showSpinner = function () {
    const spinner = document.getElementById('spinner');
    spinner.classList.add('custom-loader');
  }
  /**
   * Hides the spinner when upload fetch data is received
   */
  const hideSpinner= function() {
    const spinner = document.getElementById('spinner');
    spinner.classList.remove('custom-loader');
}
  export { showSpinner, hideSpinner}