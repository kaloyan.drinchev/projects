import { favorites } from "./index.js";
import { showSpinner, hideSpinner } from "./spinnerOnOff.js";
const ApiKey = "HEnxHwX5HCfMrJRiaWPdqS2Tq1sk0AEX";
let arrayContainingUploadedGIFs =
  JSON.parse(window.localStorage.getItem("uploadedGIFs")) || [];
/**
 *
 * @returns {Promise<void>}the requested trending GIFs from Giphy's database.
 */

const fetchTrendingGIFs = async () => {
  const fetchTrendingData = await (
    await fetch(
      `https://api.giphy.com/v1/gifs/trending?api_key=${ApiKey}&limit=10&offset=0&rating=g&lang=en`
    )
  ).json();
  return fetchTrendingData;
};

/**
 * Fetches a list of searched GIFs from the Giphy API based on user input.
 *
 * @async
 * @function
 * @returns {Promise<Object>} A Promise that resolves with the searched GIF data.
 * @throws {Error} If there's an error during the fetch operation or data processing.
 */
const fetchSearchedGifs = async () => {
  const userInput = document.getElementById("search-field").value;
  const searchedData = await (
    await fetch(
      `https://api.giphy.com/v1/gifs/search?api_key=${ApiKey}&q=${userInput}&limit=10&offset=0&rating=g&lang=en`
    )
  ).json();
  return searchedData;
};
/**
 *
 *
 * @async
 * @function
 * @returns {Promise<Object>} A Promise that resolves with the favorite GIF data.
 * @throws {Error} If there's an error during the fetch operation or data processing.
 */
const fetchFavoritesGIFs = async () => {
  const fetchFavoritesData = await (
    await fetch(
      `https://api.giphy.com/v1/gifs?api_key=${ApiKey}&ids=${favorites}`
    )
  ).json();
  return fetchFavoritesData;
};
/**
 * Fetches a random GIF from the Giphy API.
 *
 * @async
 * @function
 * @returns {Promise<Object>} A Promise that resolves with the random GIF.
 * @throws {Error} If there's an error during the fetch operation or data processing.
 */
const fetchRandomGif = async () => {
  const fetchRandom = await (
    await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${ApiKey}`)
  ).json();
  return fetchRandom;
};

/**
 *
 * @param {object} file containing file information and properties which will be used to upload the requested GIF.
 * @returns {Array}An array full of objects which contain each uploaded GIF from the user.
 *
 * The function has two goals. The first is to post the provided GIF to Giphy's database. After that , we make another request
 * in order to retrieve the GIF using his unique ID. Lastly, we push the collected file in our local storage and return it back.
 */
const fetchUploadGIF = async (file) => {
  showSpinner();
  const newForm = new FormData();
  newForm.append("file", file);
  let URL = `https://upload.giphy.com/v1/gifs?api_key=HEnxHwX5HCfMrJRiaWPdqS2Tq1sk0AEX`;
  const result = await fetch(URL, {
    method: "POST",
    body: newForm,
  });

  const fetchResponse = await result.json();
  const fetchedID = fetchResponse.data.id;
  const fetchUploadedData = await (
    await fetch(
      `https://api.giphy.com/v1/gifs?ids=${fetchedID}&api_key=${ApiKey}`
    )
  ).json();
  arrayContainingUploadedGIFs.push(await fetchUploadedData.data[0]);
  window.localStorage.setItem(
    "uploadedGIFs",
    JSON.stringify(arrayContainingUploadedGIFs)
  );
  hideSpinner();
  return [arrayContainingUploadedGIFs[arrayContainingUploadedGIFs.length - 1]];
};
/**
 * Fetches and displays details of a GIF using its ID from the Giphy API.
 *
 * @async
 * @param {string} gifId - The ID of the GIF to retrieve details for.
 * @returns {Promise<void>} - A Promise that resolves when the GIF details are fetched and displayed.
 * @throws {Error} If there's an error during the fetch operation or data processing.
 */
const getGifDetails = async (gifId) => {
  try {
    const response = await fetch(
      `https://api.giphy.com/v1/gifs/${gifId}?api_key=${ApiKey}`
    );
    const data = await response.json();
    const gifDetails = data.data;

    const titleElement = document.createElement("h2");
    titleElement.textContent = gifDetails.title;

    const usernameElement = document.createElement("p");
    usernameElement.textContent = `Username: ${gifDetails.username}`;

    const urlElement = document.createElement("p");
    urlElement.textContent = `URL: ${gifDetails.url}`;

    const uploadDateElement = document.createElement("p");
    uploadDateElement.textContent = `Upload Date: ${gifDetails.import_datetime}`;

    const sourceElement = document.createElement("p");
    sourceElement.textContent = `Source: ${gifDetails.source}`;
    const gifDetailsContainer = document.getElementById("gifDetailsContainer");
    gifDetailsContainer.innerHTML = "";
    gifDetailsContainer.appendChild(titleElement);
    gifDetailsContainer.appendChild(usernameElement);
    gifDetailsContainer.appendChild(urlElement);
    gifDetailsContainer.appendChild(uploadDateElement);
    gifDetailsContainer.appendChild(sourceElement);
    const gifThumbnail = document.querySelector(`[id='${gifId}']`);
    gifThumbnail.addEventListener("mouseenter", () => {
      gifDetailsContainer.style.display = "block";
    });
    gifThumbnail.addEventListener("mouseleave", () => {
      gifDetailsContainer.style.display = "none";
    });
  } catch (error) {
    console.error(error);
  }
};

export {
  fetchTrendingGIFs,
  fetchUploadGIF,
  arrayContainingUploadedGIFs,
  fetchSearchedGifs,
  fetchFavoritesGIFs,
  getGifDetails,
  fetchRandomGif,
};
