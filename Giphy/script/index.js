import {
  fetchTrendingGIFs,
  fetchUploadGIF,
  fetchSearchedGifs,
  fetchFavoritesGIFs,
  fetchRandomGif,
} from "./apiKey.js";
import {
  getUploadedData,
  clearSearchAndTrending,
  gifsLayout,
  handleUploadedData,
} from "./createHTMLelements.js";
import { hideSpinner } from "./spinnerOnOff.js";
const searchForm = document.querySelector(".search-bar");
const trendingTab = document.querySelector("#trending");
const uploadTab = document.querySelector("#upload");
const isFileAlreadyHere =
  JSON.parse(localStorage.getItem("uploadedData")) || [];
const favoritesTab = document.getElementById("favorites");
const fetchRandom = async () => await fetchRandomGif();
const randomGif = await fetchRandom();
const randomGifData = randomGif.data;
let favorites = JSON.parse(localStorage.getItem("favorites")) || [
  randomGifData.id,
];
/**
 * Displays the trending gifs when loading the page
 * @returns {void}
 */
const loadWindow = async () => {
  const fetchedData = await fetchTrendingGIFs();
  gifsLayout(fetchedData.data);
};
window.onload = loadWindow();

/**
 * Displays the trending gifs when choosing the Trending category
 * @returns {void}
 */
const invokeTrending = async () => {
  clearSearchAndTrending();
  const fetchedData = await fetchTrendingGIFs();
  gifsLayout(fetchedData.data);
};
/**
 *
 * @param {File} selectedFile is the GIF that the user wants to upload.
 * The function makes a request to fetchUploadGIF() to upload and collect back the GIF to Giphy's server using the response ID.
 * Next, we save the GIF in our local storage and call another function to display the uploaded GIF.
 * @throws {Error}In case the fetchUploadGIF() doesn't manage to upload the GIFs, we catch the error and display it as an alert.
 */
const handleFileChange = async (selectedFile) => {
  try {
    const fetchUploaded = await fetchUploadGIF(selectedFile);
    isFileAlreadyHere.push(selectedFile.lastModified);
    localStorage.setItem("uploadedData", JSON.stringify(isFileAlreadyHere));
    handleUploadedData(fetchUploaded);
  } catch (e) {
    hideSpinner();
    alert(`Invalid file type!\nPlease make sure to upload a .gif type file.`);
  }
};
/**
 * This function is called each time the upload menu is clicked on.
 * The first function displays the elements on the page and an event listener is added in case the upload button is triggered
 * @returns In case a file is uploaded successfully , the function calls another function to handle the upload process.
 * @throws {Error} In case you attempt to upload the same GIF twice.
 */
const invokeUpload = async () => {
  await getUploadedData();
  const uploadAGif = document.getElementById("upload-button");
  uploadAGif.addEventListener("change", async (event) => {
    const selectedFile = event.target.files[0];
    if (!isFileAlreadyHere.includes(selectedFile.lastModified)) {
      await handleFileChange(selectedFile);
    } else {
      alert("You have already uploaded this GIF!");
    }
  });
};
/**
 * when clicked on favorites category displays the favorite gifs
 * @returns {void}
 */
const invokeFavoriteGifs = async () => {
  clearSearchAndTrending();
  const favGifs = await fetchFavoritesGIFs();
  gifsLayout(favGifs.data);
};
/**
 *
 * @returns spreads the favorites array
 */
const getFavorites = () => [...favorites];
/**
 *
 * @param {string} id
 * @returns the status of the gif if its in favorites or not
 */
const renderFavoriteStatus = (id) => {
  const favorites = getFavorites();
  return favorites.includes(id)
    ? "../images/full-heart.png"
    : "../images/empty-heart.png";
};

/**
 * Fetches a list of searched GIFs from the Giphy API based on user input.
 *
 * @async
 * @function
 * @returns {Promise<Object>} A Promise that resolves with the searched GIF data.
 * @throws {Error} If there's an error during the fetch operation or data processing.
 */
const invokeSearchGifs = async () => {
  clearSearchAndTrending();
  const userInput = document.getElementById("search-field").value.trim();
  if (!userInput) {
    displayMessage("Please enter a keyword to search for GIFs.");
  } else {
    const searchedGifs = await fetchSearchedGifs(userInput);
    if (searchedGifs.data.length === 0) {
      displayMessage("No GIFs found for the given keyword.");
    } else {
      hideMessage();
      gifsLayout(searchedGifs.data);
    }
  }
};

const searchMessage = document.getElementById("search-message");

/**
 * Displays a message on the UI.
 *
 * @param {string} message - The message to be displayed.
 * @returns {void}
 */
const displayMessage = (message) => {
  searchMessage.textContent = message;
  searchMessage.style.fontSize = "40px";
  searchMessage.style.fontWeight = "bold"
  searchMessage.style.display = "block";
};

/**
 * Hides the message on the UI.
 *
 * @returns {void}
 */
const hideMessage = () => {
  searchMessage.textContent = "";
  searchMessage.style.display = "none";
};

searchForm.addEventListener("submit", (event) => {
  event.preventDefault();
  invokeSearchGifs();
});

trendingTab.addEventListener("click", invokeTrending);
uploadTab.addEventListener("click", invokeUpload);
favoritesTab.addEventListener("click", invokeFavoriteGifs);

export { renderFavoriteStatus, favorites };
