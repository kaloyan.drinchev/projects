import { arrayContainingUploadedGIFs } from "./apiKey.js";
import { renderFavoriteStatus, favorites } from "./index.js";
import { getGifDetails } from "./apiKey.js";

const resultField = document.getElementById("search-results");
const trendGIFbox = document.getElementById("gif-layout");
const searchButton = document.getElementById("search-button");
const searchField = document.getElementById("search-field");
const allGifsLayout = document.querySelector(".trending-GIFs");
const heartButton = document.getElementById("heart-button");
/**
 *
 * @param {Array} data fetches the GIFs that the user has uploaded and displays them of the screen.
 * In case the user does not have any uploaded GIFs, the screen displays a default message.
 */
const handleUploadedData = async function(data) {
  resultField.innerHTML = "";

  const newElement = document.createElement("h3");
  newElement.innerHTML = "Your Uploaded GIFs:";
  newElement.id = "uploaded-GIFs-title";
  const uploadedGIFsContainer = document.createElement("div");
  uploadedGIFsContainer.id = "uploaded-gifs-container";
  resultField.appendChild(newElement);
  resultField.appendChild(uploadedGIFsContainer);
  if (data.length === 0) {
    const noUploads = document.createElement("div");
    noUploads.innerHTML = "You do not have any uploaded GIFs so far.😢";
    noUploads.id = "no-upload-message";
    uploadedGIFsContainer.appendChild(noUploads);
  } else {
    gifsLayout(data);
  }
};

/**
 * Allows the user to upload a gif on the Upload menu by creating an upload button.
 * @function calls handleUploadedData(arrayContainingUploadedGIFs) in order to display any uploaded GIFs if there are any.
 */
const getUploadedData = async () => {
  clearSearchAndTrending();

  const uploadContainer = document.createElement("div");
  uploadContainer.classList.add("upload-container");

  const uploadTitle = document.createElement("h1");
  uploadTitle.innerHTML = "Upload a GIF:";
  uploadTitle.id = "upload-title";

  const uploadButton = document.createElement("input");
  uploadButton.type = "file";
  uploadButton.id = "upload-button";

  uploadContainer.appendChild(uploadTitle);
  uploadContainer.appendChild(uploadButton);

  trendGIFbox.appendChild(uploadContainer);
  handleUploadedData(arrayContainingUploadedGIFs);
};
/**
 * @param {string} currentDivGifId
 * Pushes the current liked gif into favorites
 */
const addToFavorites = (currentDivGifId) => {
  favorites.push(currentDivGifId);
  localStorage.setItem("favorites", JSON.stringify(favorites));
};
/**
 * @param {string} currentDivGifId
 * Removes the current liked gif into favorites
 */
const removeFromFavorites = (currentDivGifId) => {
  const index = favorites.indexOf(currentDivGifId);

  if (index !== -1) {
    favorites.splice(index, 1);
  }
  localStorage.setItem("favorites", JSON.stringify(favorites));
};
/**
 *
 * @param {string} currentElement
 * Toggles the heart image depending of its status
 */
const toggleHeartImage = (currentElement) => {
  let img = document.getElementById("heart-button" + currentElement).src;

  if (img.indexOf("empty-heart.png") !== -1) {
    document.getElementById("heart-button" + currentElement).src =
      "../images/full-heart.png";
    addToFavorites(currentElement);
  } else {
    document.getElementById("heart-button" + currentElement).src =
      "../images/empty-heart.png";
    removeFromFavorites(currentElement);
  }
};
/**
 * UI of the gifs
 * @param {Array} data
 * Displays the gifs interface
 */
const gifsLayout = async (data) => {
  // localStorage.clear();
  data.forEach((element) => {
    const divListItem = document.createElement("div");
    divListItem.setAttribute("class", "gif-heart-container");
    divListItem.setAttribute("id", `${element.id}`);

    const gifImage = document.createElement("img");
    gifImage.setAttribute("src", element.images.original.url);
    gifImage.setAttribute("class", "position-gif-in-container");
    gifImage.addEventListener("mouseenter", () => {
      getGifDetails(element.id);
    });
    gifImage.setAttribute("src", element.images.original.url);
    gifImage.setAttribute("class", "position-gif-in-container");

    const heartImage = document.createElement("input");
    heartImage.setAttribute("class", "position-heart-in-container");
    heartImage.setAttribute("id", "heart-button" + element.id);
    heartImage.setAttribute("type", "image");
    heartImage.setAttribute("src", renderFavoriteStatus(element.id));
    heartImage.addEventListener("click", () => toggleHeartImage(element.id));

    divListItem.append(gifImage);
    divListItem.append(heartImage);
    allGifsLayout.appendChild(divListItem);
  });
};
/**
 * clears the screen for in order to generate new content upon request.
 */
const clearSearchAndTrending = () => {
  document.getElementById("search-results").innerHTML = "";
  document.getElementById("gif-layout").innerHTML = "";
  document.getElementById("search-message").innerHTML = "";
};
export {
  heartButton,
  searchButton,
  searchField,
  getUploadedData,
  clearSearchAndTrending,
  gifsLayout,
  handleUploadedData,
  resultField,
};
